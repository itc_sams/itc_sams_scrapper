# -*- coding: utf-8 -*-
"""
Author: Samuel Ortmans
Author: Samuel Guerchonovitch
"""
import argparse as ap
from playstorescrapper import PlayStoreScrapper
from google_play_scraper import app as gps_app
# import play_scraper



DEBUG = False
QUICK_DEBUG = False
PARSER_DEBUG = True


def parse_arguments():
    """
    sets the arguments of the .py file with parameters
    :return: verbose, driver_pause_time, db_name, csv_filename, headless, update, debug
    """
    parser = ap.ArgumentParser(description= 'Process some commands on the code')
    parser.add_argument('-v', '--verbose', action='store_true', default=False)
    parser.add_argument('-dpt', '--driver_pause_time', default=0.5, type=float, nargs=1)
    parser.add_argument('--db', type=str, default='')
    parser.add_argument('--csv', type=str, default='')
    parser.add_argument('-head', '--headless', action="store_true", default=False)
    parser.add_argument('-u', '--update', action="store_true", default=False)
    parser.add_argument('-d', '--debug', action="store_true", default=False)
    args = parser.parse_args()
    verbose = args.verbose
    debug = args.debug
    driver_pause_time = args.driver_pause_time
    db_name = args.db
    csv_filename = args.csv
    headless = args.headless
    update = args.update
    return verbose, driver_pause_time, db_name, csv_filename, headless, update, debug


def main():
    verbose, driver_pause_time, db_filename, csv_filename, headless, update, debug = parse_arguments()
    if debug:
        return
    scrapper = PlayStoreScrapper(headless=headless,
                                 driver_pause_time=driver_pause_time,
                                 csv_filename=csv_filename,
                                 verbose=verbose)
    if update:
        scrapper.update()
    else:
        scrapper.scrap_all()


def tests():
    # print('************************************************')
    # print('               google-play-scraper')
    # print('************************************************')
    # result = gps_app(
    #     'app.over.editor',
    #     lang='en',  # defaults to 'en'
    #     country='us'  # defaults to 'us'
    # )
    # for key, value in result.items():
    #     print(f'{key}: {value}')

    # print('************************************************')
    #     # print('                  play-scraper')
    #     # print('************************************************')
    #     # result = play_scraper.search('pokemon', page=12, detailed=True)
    #     # for i in result:
    #     #     for key, value in i.items():
    #     #         print(f'{key}: {value}')

    verbose, driver_pause_time, db_filename, csv_filename, headless, update, debug = parse_arguments()

    if not debug:
        return
    print('************************************************')
    print('*                   TEST                       *')
    print('************************************************')
    headless = True
    csv_filename = 'data.csv'
    db_name = 'test_table'
    verbose = True
    scrapper = PlayStoreScrapper(headless=headless,
                                 csv_filename=csv_filename,
                                 db_name=db_name,
                                 verbose=verbose)
    scrapper._parser_debug()
    del scrapper
    print('************************************************')


if __name__ == '__main__':
    tests()
    main()
