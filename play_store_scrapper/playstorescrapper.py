# -*- coding: utf-8 -*-
"""
Author: Samuel Ortmans
Author: Samuel Guerchonovitch
"""

import os
import re
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import numpy as np
from selenium.common import exceptions as selex
from tqdm.std import tqdm
from sql_handler import SQLHandler
import mysql
import mysqlconfig as cfg
import uclassifyconfig as ucc
import requests

PARSER_DEBUG = False
QUICK_DEBUG = False


class PlayStoreApp:
    NAME_CLASS = 'AHFaub'
    DESCRIPTION_CLASS = 'W4P4ne'
    PEGI_CLASS = 'KmO8jd'
    SCORE_REPARTITION = 'VEF2C'
    NB_VOTE_CLASS = 'AYi5wd.TBRnV'
    STARS_CLASS = 'vQHuPe.bUWb7c'
    SCORE_DIV_CLASS = 'dNLKff'
    EDITOR_CHOICE_CLASS = 'vWpEff'
    EXTRA_INFO = 'hAyfc'
    LAST_UPDATE = 0
    INSTALLATIONS = 2
    VERSION = 3
    EMOJI_PATTERN = re.compile(r"[\U00010000-\U0010FFFF]", re.VERBOSE)
    # EMOJI_PATTERN = re.compile(r"[\U0001F600-\U0001F64F]|[\U0001F300-\U0001F5FF]|[\U0001F680-\U0001F6FF]|"
    #                            r"[\U0001F1E0-\U0001F1FF]")

    def __init__(self, driver, url=None):
        self.driver = driver
        self.url = url
        self.data = dict()
        self.data['app_id'] = ''
        self.data['name'] = ''
        self.data['pegi'] = ''
        self.data['is_editor_choice'] = False
        self.data['score'] = 0
        self.data['nb_vote_total'] = 0
        self.data['nb_vote'] = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
        self.data['last_update'] = ''  # datetime.datetime(1970, 1, 1)
        self.data['version'] = ''
        self.data['android_version_min'] = ''
        self.data['nb_download'] = 0
        self.data['description'] = ''

    def get_body(self):
        """
        scrolls the web page to the bottom and return the HTML
        :return:
        """
        self.driver.get(self.url)
        return self.driver.find_element_by_tag_name('body')

    def parse(self):
        '''
        Parses the url link and the html page to return the data
        :return: the parsed html page
        '''
        if self.url:
            self.data['app_id'] = self.url.split('details?id=')[1]
            if PARSER_DEBUG:
                print(f"App ID: {self.data['app_id']}")
            html = self.get_body()
            self._parse_html(html)
            return self.data
        return None

    def _parse_html(self, html):
        """
        given the html page of an app being parsed, picks up the attributes
        :param html:
        :return:
        """
        self.get_name(html)
        self.get_pegi(html)
        self.get_is_team_choice(html)
        self.get_score(html)
        self.get_score_repartition(html)
        self.get_extra_information(html)
        self.get_description(html)
        pass

    def get_name(self, html):
        """
        set the name of our app object
        :type html: selenium.webdriver.remote.webelement.WebElement
        """
        try:
            self.data['name'] = html.find_element_by_class_name(PlayStoreApp.NAME_CLASS) \
                .find_element_by_tag_name('span') \
                .get_attribute('innerHTML')
            self.data['name'] = self.EMOJI_PATTERN.sub('', self.data['name'])
        except selex.NoSuchElementException:
            self.data['name'] = ''
        if PARSER_DEBUG:
            print(f"App name: {self.data['name']}")

    def get_description(self, html):
        """
        set the name of our app object
        :type html: selenium.webdriver.remote.webelement.WebElement
        """
        try:
            self.data['description'] = html.find_element_by_class_name(PlayStoreApp.DESCRIPTION_CLASS)\
                .find_element_by_tag_name('meta')\
                .get_attribute('content')
            self.data['description'] = self.EMOJI_PATTERN.sub('', self.data['description'])
            # self.data['description'] = self.data['description'].encode(encoding='UTF-8', errors='ignore')
        except selex.NoSuchElementException:
            self.data['description'] = ''
        if PARSER_DEBUG:
            print(f"Description: {self.data['description']}")

    def get_is_team_choice(self, html):
        """
        set the boolean 'team choice' of our app object
        :type html: selenium.webdriver.remote.webelement.WebElement
        """
        try:
            html.find_element_by_class_name(PlayStoreApp.EDITOR_CHOICE_CLASS)
            self.data['is_editor_choice'] = True
        except selex.NoSuchElementException:
            self.data['is_editor_choice'] = False
        if PARSER_DEBUG:
            print(f"Editor Choice: {self.data['is_editor_choice']}")

    def get_score(self, html):
        """
        set the total score of our app object
        :type html: selenium.webdriver.remote.webelement.WebElement
        """
        try:
            self.data['nb_vote_total'] = int(html.find_element_by_class_name(PlayStoreApp.NB_VOTE_CLASS)
                                             .find_element_by_tag_name('span')
                                             .get_attribute('innerHTML').replace(',', '').replace('\u202f', ''))
            score_div = html.find_element_by_class_name(PlayStoreApp.SCORE_DIV_CLASS) \
                .find_elements_by_class_name(PlayStoreApp.STARS_CLASS)
            self.data['score'] = 0
            for s in score_div:
                s = s.get_attribute('style')
                if s:
                    self.data['score'] += float(re.sub(r'.*width: (.*?)%.*', r'\1', s)) / 100
                else:
                    self.data['score'] += 1

            if PARSER_DEBUG:
                print(f"score: {self.data['score']}")
                print(f"nb vote: {self.data['nb_vote_total']}")
        except selex.NoSuchElementException:
            self.data['score'] = 0
            self.data['nb_vote_total'] = 0

    def get_score_repartition(self, html):
        """
        set the score repartition between 1 to 5 of our app object
        :type html: selenium.webdriver.remote.webelement.WebElement
        """

        try:
            score_divs = html.find_element_by_class_name(PlayStoreApp.SCORE_REPARTITION) \
                .find_elements_by_tag_name('div')
            total = 0
            self.data['nb_vote'] = {}
            for score_div in score_divs:
                spans = score_div.find_elements_by_tag_name('span')
                span_value = int(spans[0].get_attribute('innerHTML'))
                span_percentage = spans[1].get_attribute('style')
                span_percentage = int(re.sub(r'.*width: (.*?)%.*', r'\1', span_percentage))
                self.data['nb_vote'][span_value] = span_percentage
                total += span_percentage
            for key in self.data['nb_vote'].keys():
                self.data['nb_vote'][key] = round(self.data['nb_vote'][key]
                                                  * self.data['nb_vote_total']
                                                  / total)
                if PARSER_DEBUG:
                    print(f"{key}: {self.data['nb_vote'][key]} votes")

        except selex.NoSuchElementException:
            self.data['nb_vote'] = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
            if PARSER_DEBUG:
                print('Score Repartition: NO SCORE REPARTITION')

    def get_extra_information(self, html):
        """
        set the current version, the last update and the count of downloads of our app object
        :type html: selenium.webdriver.remote.webelement.WebElement
        """
        extra_info_dict = {}
        try:
            extra_info = html.find_elements_by_class_name(PlayStoreApp.EXTRA_INFO)

            for index, div in enumerate(extra_info):
                new_div = div.find_element_by_tag_name('div')
                criteria = new_div.get_attribute('innerHTML')
                new_span = div.find_element_by_tag_name('span') \
                    .find_element_by_tag_name('div') \
                    .find_element_by_tag_name('span')
                value = new_span.get_attribute('innerHTML')

                if index == PlayStoreApp.LAST_UPDATE:
                    self.data['last_update'] = value
                    if PARSER_DEBUG:
                        print(f'{criteria}: {value}')
                elif index == PlayStoreApp.VERSION:
                    self.data['version'] = value
                    if PARSER_DEBUG:
                        print(f'{criteria}: {value}')
                elif index in [PlayStoreApp.INSTALLATIONS]:
                    value = value.replace('&nbsp;', '')
                    if re.match('\+', value):
                        value = int(value.replace('+', ''))
                        self.data['nb_download'] = value
                        if PARSER_DEBUG:
                            print(f'{criteria}: {value}+')
                    elif PARSER_DEBUG:
                        print(f'{criteria}: {value}')

                    extra_info_dict[criteria] = value
        except selex.NoSuchElementException:
            if PARSER_DEBUG:
                print('Criteria: NO CRITERIA')

        return extra_info_dict

    def get_pegi(self, html):
        """
        set the pegi attribute of our app object
        :type html: selenium.webdriver.remote.webelement.WebElement
        """
        try:
            self.data['pegi'] = re.sub(r'.*(PEGI |>)(.*)', r'\2',
                                       html.find_element_by_class_name(PlayStoreApp.PEGI_CLASS)
                                       .get_attribute('innerHTML'))
            if PARSER_DEBUG:
                print(f"PEGI: {self.data['pegi']}")
        except selex.NoSuchElementException:
            self.data['pegi'] = np.nan


class PlayStoreScrapper:
    PARSER_DEBUG_URLS = ['https://play.google.com/store/apps/details?id=air.com.games2win.internationalfashionstylist',
                         'https://play.google.com/store/apps/details?id=air.com.noxgames.PuppetSoccer2014',
                         'https://play.google.com/store/apps/details?id=com.trello',
                         'https://play.google.com/store/apps/details?id=com.facebook.katana',
                         'https://play.google.com/store/apps/details?id=abadis.dic']
    URL_PLAY_STORE = 'https://play.google.com'
    URL_CATEGORIES = 'https://play.google.com/store/apps/'
    ENGLISH_LANGUAGE = 'hl=en'
    COLUMN_NAMES = ['app_id', 'name', 'pegi', 'is_editor_choice', 'score', 'nb_vote_total', 'nb_vote',
                    'last_update',
                    'version', 'android_version_min', 'nb_download']

    def __init__(self, driver_pause_time=0.5, headless=True, db_name='google_play_store', csv_filename=None,
                 verbose=False):
        self.headless = headless
        self.db_name = db_name
        self.db = SQLHandler(self.db_name)
        self.csv_filename = csv_filename
        self.verbose = verbose
        self._driver = None
        self.initialize_driver()
        self._driver_pause_time = driver_pause_time
        self.psa = PlayStoreApp(self._driver)
        self.categories_urls = []
        self.apps_urls = []

    def initialize_driver(self):
        """
        returns a selenium _driver
        :return:
        """
        if os.name == 'nt':
            print('Windows')
            chromedriver_path = "./chromedriver.exe"
        else:
            print('Unix')
            chromedriver_path = './chromedriver'
        
        options = Options()
        if self.headless:
            options.add_argument('--headless')
        self._driver = webdriver.Chrome(options=options, executable_path=chromedriver_path)
        if PARSER_DEBUG or QUICK_DEBUG:
            self._driver.minimize_window()

    def call_post_api(self, description, mode, api_token):
        '''
        Requests an API POST to uClassify, an API web service in order to provide with the most probable taxonomy of the
        given data. We will use it for the description of the app as well as for the comments.
        :param data: the string to study
        :param api_token: the api used for the site
        :return: a dictionnary containing the 3 most probable answers
        '''
        response = requests.post(f'https://api.uclassify.com/v1/uclassify/{mode}/classify',
                                 data=f"{{\"texts\": [\"{description}\"]}}".encode('utf-8'),
                                 headers={'Authorization': 'Token ' + api_token})
        # Let's get only the 3 most probable categories
        my_list = response.json()
        class_names = sorted(my_list[0]['classification'], key=lambda x: x['p'], reverse=True)[:3]
        dict_result = {i['className']: i['p'] for i in class_names}

        return str(dict_result)


    def update(self):
        """
        When an update is to be triggered, fuel the infos table with the updated informations on the scrapped apps
        :return:
        """
        # We're retrieving the config information
        app_ids = self.select_app_ids_from_db()
        tqdm_app_id = tqdm(app_ids.items())
        nb_err = 0
        for app_id, description in tqdm_app_id:
            tqdm_app_id.set_postfix(id=app_id, nb_err=nb_err)
            if description:
                try:
                    taxonomy_score = self.call_post_api(description, ucc.MODES['taxonomy'], ucc.API_KEY)
                except KeyError as err:
                    nb_err += 1
                    taxonomy_score = ''
                self.update_taxonomy(app_id, taxonomy_score)
                self.db.db.commit()

    def scrap_all(self):
        """
        scrap all categories to create all app objects and setting their attributes with above functions
        :return:
        """
        self.get_categories_urls()
        self.get_apps_url()
        if self.verbose:
            print(f'1/{len(self.apps_urls)}: {self.apps_urls[0]}')
            print(f'{len(self.apps_urls)}/{len(self.apps_urls)}: {self.apps_urls[-1]}')
        self.get_data()
        pass  # TODO

    def scroll_to_bottom(self):
        """
        scrolls the page to the bottom
        """
        last_height = self._driver.execute_script("return document.body.scrollHeight")
        while True:
            self._driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(self._driver_pause_time)
            new_height = self._driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height

    def get_full_url(self, cat):
        if '?' in cat:
            return f"{self.URL_PLAY_STORE}{cat}&{self.ENGLISH_LANGUAGE}"
        else:
            return f"{self.URL_PLAY_STORE}{cat}?{self.ENGLISH_LANGUAGE}"

    def get_categories_urls(self):
        """
        returns the list of all categories_urls' url
        """
        html = self.read_page(self.URL_CATEGORIES)
        category = re.findall('"/store/apps/category/.*?"', html)
        ret = []
        for elem in tqdm(list(set(category)), desc='Categories URLs'):
            if r'\u003d' not in elem:
                self.categories_urls.append(self.get_full_url(elem[1:-1]))
        self.categories_urls.sort()

    def read_page(self, url):
        """
        scrolls the web page to the bottom and return the HTML
        """
        self._driver.get(url)
        self.scroll_to_bottom()
        return self._driver.page_source

    def get_apps_url(self):
        """
        returns the list of apps' url within a category
        """
        tqdm_categories_urls = tqdm(self.categories_urls, desc='Apps URLs')
        for i, url in enumerate(tqdm_categories_urls):
            tqdm_categories_urls.set_postfix(url=url)
            html = self.read_page(url)
            apps_url = re.findall('"/store/apps/details.id=.*?"', html)
            for elem in apps_url:
                self.apps_urls.append(f"{self.URL_PLAY_STORE}{elem[1:-1]}")
        self.apps_urls = sorted(list(set(self.apps_urls)))

    def get_data(self):
        """
        fuel the csv file and/or the db with the objects, externalizing its attributes as features
        :return:
        """
        tqdm_apps_urls = tqdm(self.apps_urls, desc='Apps Data')
        for i, url in enumerate(tqdm_apps_urls):
            tqdm_apps_urls.set_postfix(url=url)
            self.psa.url = url
            self.psa.parse()
            if self.db_name:
                self.write_to_db()
                if i % 10 == 0:
                    self.db.db.commit()
            if self.csv_filename:
                self.write_to_csv()
        self.db.db.commit()

    def write_to_csv(self):
        """
        fuel the csv file with the app object attributes
        :return:
        """
        with open(self.csv_filename, 'a', encoding='utf8') as file:
            for i, key in enumerate(self.COLUMN_NAMES):
                if isinstance(self.psa.data[key], dict):
                    for j, k in enumerate(self.psa.data[key].keys()):
                        file.write(str(self.psa.data[key][k]))
                        if j != len(self.psa.data[key].keys()) - 1:
                            file.write(',')
                else:
                    file.write(str(self.psa.data[key]))
                if i == len(self.COLUMN_NAMES) - 1:
                    file.write('\n')
                else:
                    file.write(',')

    def write_to_db(self):
        """
        fuel the db with the attributes of the app objects
        :return:
        """
        apps = (
        self.psa.data['app_id'], self.psa.data['name'], self.psa.data['description'], self.psa.data['description'])
        try:
            self.db.cursor.execute(cfg.insert_queries['apps'], apps)
            app_id = self.db.cursor.lastrowid
        except mysql.connector.errors.DatabaseError as err:
            print(err)
        except mysql.connector.errors.IntegrityError as err:
            self.db.cursor.execute(f"SELECT id FROM apps WHERE app_id = '{self.psa.data['app_id']}'")
            app_id = self.db.cursor.fetchall()
            app_id = int(app_id[0][0])

        pegi = self.psa.data['pegi']
        is_editor_choice = self.psa.data['is_editor_choice']
        score = self.psa.data['score']
        nb_vote_total = self.psa.data['nb_vote_total']
        nb_vote_1 = self.psa.data['nb_vote'][1]
        nb_vote_2 = self.psa.data['nb_vote'][2]
        nb_vote_3 = self.psa.data['nb_vote'][3]
        nb_vote_4 = self.psa.data['nb_vote'][4]
        nb_vote_5 = self.psa.data['nb_vote'][5]
        last_update = self.psa.data['last_update']
        version = self.psa.data['version']
        android_version_min = self.psa.data['android_version_min']
        nb_download = self.psa.data['nb_download']

        infos = (app_id, pegi, is_editor_choice, score, nb_vote_total, nb_vote_1, nb_vote_2, nb_vote_3, nb_vote_4,
                 nb_vote_5, last_update, version, android_version_min, nb_download)
        try:
            self.db.cursor.execute(cfg.insert_queries['infos'], infos)
        except Exception as err:
            err = None

    def select_app_ids_from_db(self):
        self.db.cursor.execute(cfg.select_app_id_description_update_taxonomy)
        result = self.db.cursor.fetchall()
        return {app_id[0]: app_id[1] for app_id in result}

    def update_taxonomy(self, app_id, taxonomy_score):
        self.db.cursor.execute(cfg.update_table['taxonomy'], (app_id, taxonomy_score, taxonomy_score))

    def _parser_debug(self):
        """
        test on specific url for debug
        :return:
        """
        print('ENTERING PARSER DEBUG')
        csv_filename = self.csv_filename
        db_filename = self.db_name
        self.csv_filename = '_test_playstorescrapper.csv'
        self.db_name = 'test_playstorescrapper'
        self.apps_urls = self.PARSER_DEBUG_URLS
        self.get_data()
        self.csv_filename = csv_filename
        self.db_name = db_filename

    def __del__(self):
        """
        clean the web driver
        :return:
        """
        self._driver.close()
        del self._driver
        del self.headless
        del self._driver_pause_time
        del self.psa
        del self.categories_urls
        del self.apps_urls
        del self.db_name
        del self.db
        del self.csv_filename
        del self.verbose
