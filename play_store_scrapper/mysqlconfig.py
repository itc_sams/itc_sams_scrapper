# All information about the DB are in the config file.


# The db is  created locally, hence make sure to have a local SQL server with the correct configuration
mysql = {'host': 'localhost',
         'user': 'root',
         'password': 'root',
         'port': 3306}

init = ['SET NAMES utf8mb4', 'SET CHARACTER SET utf8mb4', 'SET character_set_connection=utf8mb4']

tables = {'apps': """
                  CREATE TABLE apps (
                  id int PRIMARY KEY AUTO_INCREMENT,
                  app_id VARCHAR(255) UNIQUE NOT NULL,
                  name VARCHAR(255),
                  description TEXT
                  )
                  """,
          'infos': """
                   CREATE TABLE infos (
                   id int PRIMARY KEY AUTO_INCREMENT,
                   app_id int,
                   pegi varchar(255),
                   is_editor_choice bool,
                   score float,
                   nb_vote_total int,
                   nb_vote_1 int,
                   nb_vote_2 int,
                   nb_vote_3 int,
                   nb_vote_4 int,
                   nb_vote_5 int,
                   last_update varchar(255),
                   version varchar(255),
                   android_version_min varchar(255),
                   nb_download int,
                   UNIQUE KEY (app_id, is_editor_choice, score, nb_vote_total, last_update, version, nb_download),
                   FOREIGN KEY (app_id) REFERENCES apps(id)
                   )
                   """,
          'taxonomy': """
                  CREATE TABLE taxonomy (
                  id int PRIMARY KEY AUTO_INCREMENT,
                  app_id INT UNIQUE KEY,
                  taxonomy_score VARCHAR(255),
                  FOREIGN KEY (app_id) REFERENCES apps(id)
                  )
                  """,
          }

insert_queries = {'apps': "INSERT INTO apps (app_id, name, description) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE description=%s",
             'infos': "INSERT INTO infos (app_id, pegi, is_editor_choice, score,\
              nb_vote_total, nb_vote_1, nb_vote_2, nb_vote_3, nb_vote_4, nb_vote_5,\
              last_update, version, android_version_min, nb_download) \
              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
             }

select_app_id_description_update_taxonomy = """
                                            SELECT DISTINCT apps.id, apps.description
                                            FROM apps
                                            WHERE apps.id NOT IN (SELECT app_id FROM taxonomy)
                                                AND apps.description IS NOT NULL
                                            UNION ALL
                                            SELECT DISTINCT apps.id, apps.description
                                            FROM apps
                                            JOIN taxonomy
                                                ON apps.id = taxonomy.app_id
                                            WHERE taxonomy.taxonomy_score = ''
                                            """

update_table = {'apps': """
                        INSERT INTO apps (id, app_id, description) VALUES(%s, %s, %s) ON DUPLICATE KEY UPDATE    
                        description=%s
                        """,
                'infos': '',
                'taxonomy': """
                            INSERT INTO taxonomy (app_id, taxonomy_score) VALUES(%s, %s) ON DUPLICATE KEY UPDATE    
                            taxonomy_score=%s
                            """
                }
