# -*- coding: utf-8 -*-
"""
Author: Samuel Ortmans
Author: Samuel Guerchonovitch
"""
import mysql.connector
from mysql.connector import errorcode
import mysqlconfig as cfg

class SQLHandler:
    def __init__(self, db_name):
        self.db = None
        self.cursor = None
        self.db_name = db_name
        self.connect()
        self.init()
        self.create_tables()

    def connect(self):
        """
        Initialization of the handler. Sets its cursor
        :return:
        """
        try:
            self.db = mysql.connector.connect(
                host=cfg.mysql['host'],
                user=cfg.mysql['user'],
                passwd=cfg.mysql['password'],
                port=cfg.mysql['port'],
                use_unicode=True
            )
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            else:
                print(err)
            exit(1)
        self.cursor = self.db.cursor()


        try:
            self.cursor.execute(f"USE {self.db_name}")
            print(f"Database {self.db_name} accessed successfully.")
        except mysql.connector.Error as err:
            print(f"Database {self.db_name} does not exists.")
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                self.create_database()
                print(f"Database {self.db_name} created successfully.")
                self.db.database = self.db_name
            else:
                print(err)
                exit(1)

    def create_database(self):
        """
        Initialize the db
        :return:
        """
        try:
            self.cursor.execute(
                f"CREATE DATABASE {self.db_name} DEFAULT CHARACTER SET 'utf8'")
        except mysql.connector.Error as err:
            print(f"Failed creating database: {err}")
            exit(1)

    def init(self):
        for query in cfg.init:
            try:
                print(f'{query}: ', end='')
                self.cursor.execute(query)
            except mysql.connector.Error as err:
                print(err.msg)
            else:
                print("OK")

    def create_tables(self):
        """
        Requests the creation of the tables with the appropriate columns
        :return:
        """
        for table in cfg.tables:
            query = cfg.tables[table]
            try:
                print(f"Creating table {table}: ", end='')
                self.cursor.execute(query)
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                    print("already exists.")
                else:
                    print(err.msg)
            else:
                print("OK")

    def __del__(self):
        """
        Forces to commit before closing correctly the db
        :return:
        """
        # self.db.commit()
        # self.db.close()
        pass



if __name__ == '__main__':
    con = SQLHandler('test_db_connect_from_python')
    input('Press ENTER to exit.')
    del con
